# UNSL TV Backend

## Descripción

Servicio WEB que actua como middleware entre un cliente y la API de [YouTube](http://youtube.com). Las peticiones 'GET' son almacenadas en un cache, evitando así consumir la pequeña cuota ofrecida por la API original.

## Instalación

```bash
$ npm install
```

## Ejecutar la aplicación

```bash
# desarrollo
$ npm run start

# recarga automática
$ npm run start:dev

# producción
$ npm run start:prod
```

Este proyecto utiliza la licencia [MIT](LICENSE).
