import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AppController } from './app.controller';
import { HttpCachedService } from './http-cached.service';
import { YoutubeLiveService } from './youtube-live.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, "..", "static"),
    }),
    HttpModule
  ],
  controllers: [AppController],
  providers: [HttpCachedService, YoutubeLiveService],
})
export class AppModule { }
