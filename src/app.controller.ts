import { Controller, Get, Param, Query } from '@nestjs/common';
import { HttpCachedService } from './http-cached.service';
import { YoutubeLiveService } from './youtube-live.service';
import { Observable } from 'rxjs';

@Controller('api')
export class AppController {
  constructor(
    private http: HttpCachedService,
    private youtube: YoutubeLiveService
  ) { }

  @Get('liveBroadcast')
  getActiveBroadcastId() {
    return this.youtube.getActiveBroadcastId();
  }

  @Get(':endpoint')
  getResource(
    @Param('endpoint') endpoint: string,
    @Query() queryParams: Map<string, any>,
  ): Observable<any> {
    return this.http.get(endpoint, queryParams);
  }
}
