import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { parse } from 'node-html-parser';
import { map, Observable } from 'rxjs';

@Injectable()
export class YoutubeLiveService {
  private LIVE_URL = 'https://www.youtube.com/channel/UCZZWwoQL1ZpRU-8hdsrUpew/live';

  constructor(private http: HttpService) { }

  getActiveBroadcastId(): Observable<any> {
    return this.http.get(this.LIVE_URL).pipe(
      map(response => {
        const html = parse(response.data);
        const canonicalURLTag = html.querySelector('link[rel=canonical]');
        const canonicalURL = canonicalURLTag.getAttribute('href');
        const videoURL = new URL(canonicalURL);
        const videoId = videoURL.searchParams.get('v');
        return { videoId }
      })
    );
  }
}
