import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { from, map, Observable, of, switchMap, tap } from 'rxjs';
import * as Keyv from 'keyv';
import * as hash from 'object-hash';

@Injectable()
export class HttpCachedService {
  private BASE_URL = 'https://youtube.googleapis.com/youtube/v3/';
  private CACHE_TTL = 24 * 60 * 60 * 1000; // 24 Hr. cache
  private keyv: Keyv<any>;

  constructor(private axios: HttpService) {
    this.keyv = new Keyv('sqlite://cache/db.sqlite', {
      ttl: this.CACHE_TTL,
    });
  }

  get(endpoint: string, queryParams: Map<string, any>): Observable<any> {
    const request = { endpoint, queryParams };
    const requestHash = hash(request);

    const fromCache$ = from(this.keyv.get(requestHash));
    const fromHTTP$ = this.axios
      .get(this.BASE_URL + endpoint, { params: queryParams })
      .pipe(
        map((res) => res.data),
        tap(() => console.log('[LOG] Executed HTTP request on ' + new Date())),
        tap((data) => this.keyv.set(requestHash, data)),
      );

    return fromCache$.pipe(switchMap((data) => (data ? of(data) : fromHTTP$)));
  }
}
